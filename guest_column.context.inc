<?php
/**
 * @file
 * guest_column.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function guest_column_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'guest_column_blocks';
  $context->description = 'Block displaying Guest columnist on node pages';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'guest_column' => 'guest_column',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'views' => array(
      'values' => array(
        'guest_column_teasers_page' => 'guest_column_teasers_page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-navigation' => array(
          'module' => 'system',
          'delta' => 'navigation',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-guest_columnist_block-block' => array(
          'module' => 'views',
          'delta' => 'guest_columnist_block-block',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'views-ad_block_nodequeues-block_1' => array(
          'module' => 'views',
          'delta' => 'ad_block_nodequeues-block_1',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'views-c0f241121e7939023138c07d96b727d6' => array(
          'module' => 'views',
          'delta' => 'c0f241121e7939023138c07d96b727d6',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Block displaying Guest columnist on node pages');
  $export['guest_column_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'on_guest_columns_teasers_page';
  $context->description = 'Blocks that appear on /guest-columns page';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'guest-columns' => 'guest-columns',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-ad_block_nodequeues-block_1' => array(
          'module' => 'views',
          'delta' => 'ad_block_nodequeues-block_1',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-c0f241121e7939023138c07d96b727d6' => array(
          'module' => 'views',
          'delta' => 'c0f241121e7939023138c07d96b727d6',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks that appear on /guest-columns page');
  $export['on_guest_columns_teasers_page'] = $context;

  return $export;
}
