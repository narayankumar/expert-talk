
build2014092101
- views changes
  - comments field in teasers view made into a link

build2014091901
- added opinion block for front page

build2014091802
- replaced type in teasers view with voice categories taxonomy term

build2014091801
- made title 'voices' in teasers view
- gave it class=black, h4, second word strong
- changed page manager setting to show voices/add voices/manage etc.
- new pathauto pattern for expert talk nodes using voices

build2014091602
- remove share this label
- provide comments box no ajax

7.x-1.0-dev1
- initial commit on sep 12 2014
