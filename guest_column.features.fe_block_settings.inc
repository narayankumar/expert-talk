<?php
/**
 * @file
 * guest_column.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function guest_column_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-9dd9c477a269547d1ec07a2f78299250'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '9dd9c477a269547d1ec07a2f78299250',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => -16,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-c0f241121e7939023138c07d96b727d6'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'c0f241121e7939023138c07d96b727d6',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'guest-columns',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => -9,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
